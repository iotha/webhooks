/****************************************************
* Copyright �2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

<?php
function Send($port, $file, $command)
{
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

    echo "Target port: ".$port."<BR>";
    echo "Command to send: ".$command."<BR>";

    $connection =  @socket_connect($socket, $file, $port);


    if( $connection )
    {
      echo "Connected<BR>";

      $a = socket_write($socket, $command);
      //echo $a."<BR>";

      $read = socket_read( $socket, 1024 );

      if( $read == false )
      {
          echo "Read failed<BR>";
      }
      else
      {
          echo "Target device returned: <BR>".$read;
          echo "<BR>";
      }
    }
    else
    {
        echo 'OFFLINE:' . socket_strerror(socket_last_error( $socket ));
    }

}
echo "Hello<BR>";

$device=$_GET["dev"];
$command=$_GET["com"];
$value=$_GET["val"];
$key=$_GET["key"];
$subdev=$_GET["sub"];

if(isset($_GET['com']) && isset($_GET['dev']))
{
  $file = file_get_contents('http://iotha.co.uk/local_ip_address.txt');
  echo "IP Address: ".$file."<BR>";

  $port=8080;
  $all=false;

  echo "Target device: ".$device."<BR>";

  switch ($device)
  {
    case "RPI":
       $port=8090;
       break;
    case "PC":
       $port=8081;
       break;
    case "WS1":
       $port=8082;
       break;
    case "WS2":
       $port=8083;
       break;
    case "LT":
       $port=8084;
       break;
    default:
       $all=true;
  }

  $entityBody = file_get_contents('php://input');
  $command = $command.",".$value.",".$key.",".$subdev.",".$device.",".$entityBody."\0";

  if($all == true)
  {
    Send(8090, $file, $command);
    Send(8081, $file, $command);
    Send(8082, $file, $command);
    Send(8083, $file, $command);
    Send(8084, $file, $command);
  }
  else
  {
    if($device == "RPI")
    {
      Send(8090, $file, $command);
    }
    if($device == "PC")
    {
      Send(8081, $file, $command);
    }
    if($device == "WS1")
    {
      Send(8082, $file, $command);
    }
    if($device == "WS2")
    {
      Send(8083, $file, $command);
    }
    if($device == "LT")
    {
      Send(8084, $file, $command);
    }
  }
}
else
{
   echo "FAIL: com and dev not set";
}
?>