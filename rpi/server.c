/****************************************************
* Copyright �2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <wiringPi.h>
#include "server.h"

int client_fd;

void StartServer()
{
    pthread_t tl;
    pthread_create(&tl,NULL,ServerThread,NULL);
}

void * ServerThread()
{
   struct addrinfo hints, *server;
   memset(&hints, 0, sizeof hints);

   hints.ai_family =  AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_flags = AI_PASSIVE || SOCK_NONBLOCK;
   getaddrinfo(NULL, "8090", &hints, &server);

   int sockfd = socket(server->ai_family,server->ai_socktype, server->ai_protocol);
   bind(sockfd, server->ai_addr, server->ai_addrlen);
   listen(sockfd, 100);

   struct sockaddr_storage client_addr;
   socklen_t addr_size = sizeof client_addr;

   char buffer[2048];

   while(1)
   {
      printf("Waiting for connection\n");
      client_fd = accept(sockfd,(struct sockaddr *) &client_addr, &addr_size);
      printf("Client connected\n");

      struct timeval tv;
      tv.tv_sec = 11;
      tv.tv_usec = 0;
      //timeout in seconds until the read function gives up
      setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
      //allow the address to be reused so that we can reconnect straight away
      setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int));

      if (client_fd > 0)
      {
          printf("Data received from client\n");
          while(read(client_fd, buffer, 2048) > 0)
          {
            printf("Received: %s\n", buffer);

            char *p_buf = buffer;
            char Arguments[5][128];
            char *token;
            int count = 0;
            while ((token = strsep(&p_buf, ",")) != NULL && count<5)
            {
                strcpy(Arguments[count],token);
                printf("Data at argument %d: %s\n",count,Arguments[count]);
                count++;
            }

            char returnBuffer[10];
            bool found=false;
            //Check the command
            if(strncmp("show",Arguments[COM],4) == 0)
            {
                //perform a command
                printf("Command received\n");

                //Get the value parameter
                char x = Arguments[VAL][0];
                switch(x)
                {
                    case '1':
                        printf("1 Selected\n");
                        found=true;
                        strcpy(returnBuffer,"OK\n");
                        break;
                    case '2':
                        printf("2 Selected\n");
                        found=true;
                        strcpy(returnBuffer,"OK\n");
                        break;
                    case '3':
                        printf("3 Selected\n");
                        found=true;
                        strcpy(returnBuffer,"OK\n");
                        break;
                    default:
                        printf("Invalid value\n");
                        break;
                }

            }
            if(strncmp("get",Arguments[COM],3) == 0)
            {
                printf("Get variable\n");
                if( strncmp("value1",Arguments[KEY],6)==0)
                {
                    sprintf(returnBuffer,("Value 1\n"));
                    found=true;
                }
                if( strncmp("value2",Arguments[KEY],6)==0)
                {
                    sprintf(returnBuffer,("Value 2\n"));
                    found=true;
                }
            }
            if(!found)
            {
                sprintf(returnBuffer,("FAIL\n"));
            }

            printf("Replying with: %s",returnBuffer);
            write(client_fd, returnBuffer, strlen(returnBuffer));
            fflush(stdout);
            
            printf("Closing client connection\n");
            close(client_fd);
          }

      }
  }
}

