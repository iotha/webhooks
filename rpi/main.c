/****************************************************
* Copyright �2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

#include <wiringPi.h>
#include "server.h"

int main(void)
{
    //start the app server
    StartServer();

    while(1)
    {
       delay(100);
    }
    return 1;
}
