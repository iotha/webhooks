﻿/****************************************************
* Copyright ©2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;

namespace webhook_client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Uri url = new Uri("http://your_website.com/webhooks.php");
        private void button1_Click(object sender, EventArgs e)
        {
            //Use GET when the request does not change the state of the server or perform an action
            WebClient wc = new WebClient();

            wc.QueryString.Add("com", "inf");
            wc.QueryString.Add("dev", "PC");
            wc.QueryString.Add("key", "com");
            string response = wc.DownloadString(url);
            textBox1.Text = response;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //Use POST when the request causes a change in state or action to be performed
            WebClient wc = new WebClient();
            NameValueCollection data = new NameValueCollection
            {
                ["Data1"] = "Some data",
                ["Data2"] = "Some more data"
            };

            wc.QueryString.Add("com", "msg");
            wc.QueryString.Add("val", "Hello");
            wc.QueryString.Add("dev", "PC");
            byte[] response = wc.UploadValues(url, "POST", data);
            string responseString = Encoding.UTF8.GetString(response);
            textBox1.Text = responseString;
        }
        
        string YourKey = "put your IFTTT key in this string";
        string YourEventName = "test";        
        private void button3_Click(object sender, EventArgs e)
        {
            //Call an IFTTT webhook
            string IFTTT = $"https://maker.ifttt.com/trigger/{YourEventName}/with/key/{YourKey}";
            Uri IFTTTurl = new Uri(IFTTT);
            
            WebClient wc = new WebClient();

            wc.QueryString.Add("value1", "Hello");
            string response = wc.DownloadString(IFTTTurl);
            textBox1.Text = response;
        }
    }
}
