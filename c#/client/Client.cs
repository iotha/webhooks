﻿/****************************************************
* Copyright ©2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    public partial class Client : Form
    {
        public Client()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            byte[] bytes = new byte[1024];

            try
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[1];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, int.Parse(textBox2.Text));
  
                Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    client.Connect(remoteEP);

                    textBox1.AppendText("Connected to: " + client.RemoteEndPoint.ToString() + Environment.NewLine);
                      
                    byte[] msg = Encoding.ASCII.GetBytes("msg,Hello,,,PC");

                    int bytesSent = client.Send(msg);

                    int bytesRec = client.Receive(bytes);
                    textBox1.AppendText("Received: " + Encoding.ASCII.GetString(bytes, 0, bytesRec) + Environment.NewLine);

                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unexpected exception : {0}", ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
