﻿/****************************************************
* Copyright ©2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace server
{
    public partial class Server : Form
    {
        public Server()
        {
            InitializeComponent();
        }
        const int PHP_COM = 0;
        const int PHP_VAL = 1;
        const int PHP_KEY = 2;
        const int PHP_SUB = 3;
        const int PHP_DEV = 4;

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public static void StartProcess(string PathAndFileName)
        {
            System.Diagnostics.Process.Start("CMD.exe", PathAndFileName);
        }

        Thread thread;
        private void button2_Click(object sender, EventArgs e)
        {
            thread = new Thread(StartListening);
            thread.Start();
        }

        Speech speech = new Speech();
        // Incoming data from the client.  
        public string data = null;

        public void StartListening()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the   
            // host running the application.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, int.Parse(textBox2.Text));

            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and   
            // listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.  
                while (!IsClosing)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.  
                    Socket handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.  
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    // Show the data on the console.  
                    Console.WriteLine("Text received : {0}", data);
                    String[] Args = data.Split(',');
                    for (int s = 0; s < Args.Length; s++)
                    {
                        Args[s] = Args[s].Replace("\0", String.Empty);
                    }

                    String Smsg = "Command Not Found";

                    if (Args.Length >= 5)
                    {
                        if (Args[PHP_DEV] == textBox1.Text) //check the device name
                        {
                            //check the command
                            switch (Args[PHP_COM])
                            {
                                case "msg":
                                    //hangs website until ok clicked
                                    MessageBox.Show(Args[PHP_VAL]);
                                    Smsg = "Ok";
                                    break;
                                case "alive":
                                    //do nothing, just ok will be returned
                                    Smsg = "I'm alive!";
                                    break;
                                case "say":
                                    speech.Say(Args[PHP_VAL], "0", "Microsoft Hazel Desktop");
                                    Smsg = "Ok";
                                    break;
                                case "inf":
                                    //return available commands
                                    if (Args[PHP_KEY] == "com")
                                    {
                                        Smsg = "msg, alive, say, suspend, shutdown, restart";
                                    }
                                    break;
                                case "suspend":
                                    Application.SetSuspendState(PowerState.Suspend, false, false);
                                    Smsg = "Ok";
                                    break;
                                case "shutdown":
                                    StartProcess("/C shutdown -s -f -t 0");
                                    Smsg = "Ok";
                                    break;
                                case "restart":
                                    StartProcess("/C shutdown -g -f -t 0");
                                    Smsg = "Ok";
                                    break;
                                default:
                                    Smsg = "Fail - Command not recognised";
                                    break;
                            }
                        }                  
                    }
                    else
                    {
                        Smsg = "Fail";
                    }
                    // Echo back to the client
                    byte[] msg = Encoding.ASCII.GetBytes(Smsg);
                    handler.Send(msg);

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        Boolean IsClosing = false;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsClosing = true;
            Environment.Exit(Environment.ExitCode);
        }
    }
}
