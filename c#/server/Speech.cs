﻿/****************************************************
* Copyright ©2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

using System;
using System.Speech.Synthesis;

namespace server
{
    class Speech
    {
        private SpeechSynthesizer Speak;

        public Speech()
        {
            Speak = new SpeechSynthesizer();
        }
        public void Say(string Words, string Rate, string Voice)
        {
            Speak.Rate = Convert.ToInt16(Rate);  //The Voice Speed -10 - 10                       
            Speak.SelectVoice(Voice);
        }
    }
}
