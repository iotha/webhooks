/****************************************************
* Copyright �2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

#include <stdio.h>
#include <curl/curl.h>

int main(void)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl) 
  {
    curl_easy_setopt(curl, CURLOPT_URL, "http://iotha.co.uk/webhook_call.php?com=msg&val=Hello&dev=PC");
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2);

    res = curl_easy_perform(curl);

    if(res != CURLE_OK)
    {
      fprintf(stderr, "Failed: %s\n", curl_easy_strerror(res));
    }
    curl_easy_cleanup(curl);
  }
  return 0;
}
