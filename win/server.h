/****************************************************
* Copyright �2019 IOTHA                             *
* This software is free for personal use only and   *
* may not be distributed or used commercially       *
* without permission from IOTHA.                    *
* iotha.co.uk/subsections/howdo/02-webhooks.shtml   *
****************************************************/

#ifndef _SERVER_H_
#define _SERVER_H_

void StartServer();
void * ServerThread();

enum PARAMS
{
    COM,
    VAL,
    KEY,
    SUB,
    DEV
};

#endif